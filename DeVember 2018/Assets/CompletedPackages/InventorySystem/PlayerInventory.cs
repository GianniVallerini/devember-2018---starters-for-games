﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerInventory : MonoBehaviour 
{

    #region FIELDS

    public List<ItemObject> items = new List<ItemObject>();
    public Action<List<ItemObject>> UpdateInventory;

    #endregion

    #region UNITY CALLBACKS

    void OnTriggerEnter(Collider other)
    {
        IPickUppable iPickUppable = other.GetComponent<IPickUppable>();
        if(iPickUppable != null)
        {
            iPickUppable.PickUp(this);

            UpdateHud();
        }
    }

    #endregion

    #region METHODS

    void UpdateHud()
    {
        if(UpdateInventory != null)
        {
            UpdateInventory(items);
        }
    }

    #endregion

}
