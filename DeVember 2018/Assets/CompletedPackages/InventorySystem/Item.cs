﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour , IPickUppable
{

    #region FIELDS

    public ItemObject item;

    #endregion

    #region METHODS

    public void PickUp(PlayerInventory inventory)
    {
        inventory.items.Add(item);

        Destroy(gameObject);
    }

    #endregion

}
