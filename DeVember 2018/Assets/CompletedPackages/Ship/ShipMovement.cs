﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{

    #region FIELDS

    public float maxSpeed;
    public float angularSpeed;
    public WheelControl wheelControl;
    public SailControl sailControl;
    public GameObject wind;

    Rigidbody rb;
    float currSpeed;

    #endregion

    #region UNITY CALLBACKS

    void Start()
    {
        rb = GetComponent<Rigidbody>();    
    }

    void FixedUpdate()
    {
        MoveShip();
        RotateShip();
    }

    #endregion

    #region METHODS

    void MoveShip()
    {
        currSpeed = maxSpeed * sailControl.currSailSpeed;

        rb.AddForce(transform.forward * currSpeed);

        if(rb.velocity.magnitude > maxSpeed)
        {
            rb.velocity = rb.velocity.normalized * maxSpeed;
        }
    }

    void RotateShip()
    {
        float l = currSpeed / maxSpeed;
        float currRotation = (angularSpeed * wheelControl.currRotation) * Mathf.Lerp(1, 0.5f, l);

        transform.Rotate(Vector3.up, currRotation);
    }

    #endregion

}
