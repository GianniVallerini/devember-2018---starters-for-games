﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wind : MonoBehaviour 
{

    #region FIELDS

    public float changeWindTime = 8f;
    public GameObject targetToFollow;

    float t = 0;

    #endregion

    #region UNITY CALLBACKS

    void Update()
    {

        transform.position = targetToFollow.transform.position;

        t += Time.deltaTime;
        
        if(t > changeWindTime)
        {
            transform.Rotate(Vector3.up, Random.Range(0f, 360f));
            t = 0;
        }
    }

    #endregion

    #region METHODS

    #endregion

}
