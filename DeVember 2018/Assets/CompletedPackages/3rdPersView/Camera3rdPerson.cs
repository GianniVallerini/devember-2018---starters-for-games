﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera3rdPerson : MonoBehaviour
{

    #region FIELDS

    public enum Shoulder
    {
        LEFT,
        RIGHT,
    }

    public KeyCode leftShoulder;
    public KeyCode rightShoulder;
    public float fov = 80;

    float shoulderOffset;
    Shoulder currentShoulder = Shoulder.RIGHT;

    #endregion

    #region UNITY CALLBACKS

    void Start()
    {
        shoulderOffset = transform.position.x;
        GetComponent<Camera>().fieldOfView = fov;
    }

    void Update()
    {
        if (Input.GetKeyDown(leftShoulder) && currentShoulder != Shoulder.LEFT)
        {
            SetShoulder(Shoulder.LEFT);
        }
        else if (Input.GetKeyDown(rightShoulder) && currentShoulder != Shoulder.RIGHT)
        {
            SetShoulder(Shoulder.RIGHT);
        }
    }

    #endregion

    #region METHODS

    void SetShoulder(Shoulder targetShoulder)
    {
        if(targetShoulder == Shoulder.LEFT)
        {
            StopAllCoroutines();
            StartCoroutine(MoveToShoulder(-shoulderOffset));
            currentShoulder = Shoulder.LEFT;
        }
        else if (targetShoulder == Shoulder.RIGHT)
        {
            StopAllCoroutines();
            StartCoroutine(MoveToShoulder(shoulderOffset));
            currentShoulder = Shoulder.RIGHT;
        }
    }

    IEnumerator MoveToShoulder(float valueAimed)
    {
        float t = 0;
        float maxT = 0.5f;
        float l = 0;
        float currX = transform.localPosition.x;

        while(t < maxT)
        {
            t += Time.deltaTime;
            l = t / maxT;
            transform.localPosition = new Vector3(Mathf.Lerp(currX, valueAimed, l), transform.localPosition.y, transform.localPosition.z);
            yield return null;
        }
    }

    #endregion

}
