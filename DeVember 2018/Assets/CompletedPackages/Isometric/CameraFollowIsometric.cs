﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowIsometric : MonoBehaviour
{

    #region FIELDS

    public GameObject target;
    public float distance = 5;

    #endregion

    #region UNITY CALLBACKS

    void Update()
    {
        FollowTarget();
    }

    #endregion

    #region METHODS

    void FollowTarget()
    {
        Vector3 offsetVector = new Vector3(-distance, distance + 2, -distance);

        transform.position = target.transform.position + offsetVector;
    }

    #endregion

}
