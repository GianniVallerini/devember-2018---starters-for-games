﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{

    #region FIELDS

    public MovementSideScroll movementScript;

    #endregion

    #region UNITY CALLBACKS

    void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Ground"))
        {
            SetOnGround();
        }
    }

    #endregion

    #region METHODS

    void SetOnGround()
    {
        movementScript.isOnGround = true;
    }

    #endregion

}
